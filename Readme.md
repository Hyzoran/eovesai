===========================
Eovesai Engine
===========================

Premise:

    A Game Engine intended for Strategy games, written in C++, subscribing the following philosophy:
    
    Engine-Level:

        - Source-level modularity between components to allow for flexibility and extensibility.

        - 'Machine' subsystem concept to decouple loosely-related systems.
            - 'GameLogic' Machine, different gameplay systems run in isolated environments and 
                communicate through message packets. Mechanics not requiring synchronous communications
                may safely operate for parrellelism. 
            - 'Compute Service' If a particular GameLogic Machine has a parellizable system, it may
            start its own new thread, or use a compute device, such as initialize a Vulkan compute device, 
            or even a machine over a network.

        - Network Abstraction, Network device defines the synchronization manner between clients, each
            client's Network device makes changes that client's GameLogic Machine.

        - Client Machine, User-interface, Camera, Input, operate as part of the Client Machine, decoupling
            it from the game world(s). Each Client may define viewports which maintain a Camera in the 
            gamelogic devices.

        - Resource Manager, Manages assets in memory to prevent memory duplication across
        machines. 

        - Package System, Assets, Part of the Resource Manager system. .zip-based Packages that contain 
        assets that may be referenced with PackageName.ObjectName.

    Module-Level:
    
        - Different Games-architectures are implemented with Gamemodule libraries, defining subclasses 
        for GameLogic machines, and Menu scripts for Client Device.

        - Define the modding API for new mods to include. After this point, all behavior of the engine
        is asset-driven.

    Game-Level:

        - Menuscript included with Module will probe all available packages built for the module.

    Example run:
    
    Eovesai --Game "Battlespace"
    >Engine Inits
    >Engine Loads Battlespace.so Module
    >Engine Loads Battlespace.so Menu
    >Menu loads all available Packages designed for Battlespace.so, may also dynamically load packages
    with terminal, menu function, or otherwise.
    >User uses menus to start the game or uses terminal.

Why:

    Eovesai was concepted to facilitate the 'Kyrica' game project, and also to provide a new option 
    libre strategy engines.

License:

    <Tentative>
    Eovesai will feature a GPL-like 'copyleft' software license (if not GPL itself), protecting the 
    code-transparency of the engine and its components. However, while allowing for Assets made for 
    the engine to be commercialized, royalty-free.
    
    No support for Commercial, or Non-Commercial users is obligated, nor are any circumstances caused by
    malfunct or otherwise unwanted behavior of this project to be held liable against the project or any of its developers.
    Use at your own risk.

Versioning Model:

    Major.Minor.Stable.RC
    
    Major - Implies changes to API. Not guaranteed to be source-compatible with material built against
    previous major version. Major versions below '1' are highly volatile and not production-ready.

    Minor - Release version. 

    Stable Flag - '1' for Stable release.

    Release Candidate - Testing Releases for adoption as stable.

Projects:

    eovesai_base - Engine-Level code and framework. Implements all the APIs used by the Engine, includes standard launch executable.
    eovesai_m_std - Standard Eovesai machine modules. standard asset formats.
    eovesai_editor - Editor for managing engine assets and mapping.
    
    
