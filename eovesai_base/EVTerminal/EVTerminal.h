 /*==================================================================================================
 * Eovesai Engine - 2019 Rrajigar Dragoons
 * Terminal.h - Terminal Interface
 * 
 *  The Terminal Interface is the primary access to the Engine. Commands provided by Core 
 * allow for direct manipulation of almost all system components by the user.
 * 
 * Written by Jesse 'Kyararos' Kowalik
*=================================================================================================*/ 
#pragma once

#include <chrono>
#include <functional>
#include <unistd.h>
#include <thread>
#include <atomic>
#include <mutex>
#include <csignal>
#include <string.h> //As the Terminal does not use xstl::String internally.

#include <libxstl/XTypes.h>
#include <libxstl/XArray.h>
#include <libxstl/XString.h>

//====================================================================================

class EVTerminal
{
public:
    
    EVTerminal( void (*FuncCommandIface)( const xstl::String& ) );
    virtual ~EVTerminal() = 0;
    
    virtual void Loop() = 0; //The main function. Calling loop() keeps the Terminal active.
    virtual void Log( const xstl::String& Msg, std::thread::id tid ) = 0; //Log a message. Checks if called 
//from a different thread.

    virtual void Resize( int sig ) = 0; //Handle SIGWINCH or similar events.
    virtual void Cont( int sig ) = 0; //Handle SIGCONT
    
protected:
    
    void (*m_FuncCommandIface)( const xstl::String& ); //Our output function.
    std::thread::id m_MyThread; //The thread that constructed the Terminal. Used by Log() to know whether 
//to use message queues or direct log writing.
};

//====================================================================================
//Default Terminal provides CLI Window interface for simutaneous input/output.
//Uses NCurses by default on Unix.

#include <curses.h>
#include <libxstl/XFileStream.h>

#define __DEFAULTFIELDY 2
#define __DEFAULTDIVY 1

class EVCursesTerminal : public EVTerminal
{
public:
    //Pass callback function to send input to.
    EVCursesTerminal( void (*FuncCommandIface)( const xstl::String& ), size_t FieldSize, size_t 
HistorySize );
    ~EVCursesTerminal();
    
    void Loop();
    void Log( const xstl::String& Msg, std::thread::id tid );
    
    void Resize( int sig ); //Handle SIGWINCH or similar events.
    void Cont( int sig ); //Handle SIGCONT - fix terminal rendering.
    
private:
    
    //====================================================================================
    //  Types
    //====================================================================================
    struct InputField
    {
        InputField( size_t Size );
        InputField( const InputField& Template );
        ~InputField();
        
        //Insert character at cursor, shifting all ahead of it by one. Fails if:
        //m_Data[Size-2] is not zero, indicating full buffer
        //Index is more than Size-2, indicating past the end of the buffer
        bool  AddChar( size_t Index, char C );
        //Delete character before the cursor. Fails if:
        //Cursor is at 0, indicating start of buffer.
        //Cursor is ahead of end of the buffer.
        bool  DelChar( size_t Index );
        void  Clear();
        
        size_t GetEnd() const; //Get last non-0 index.
        char* GetData();
        size_t GetSize() const;
        
        void operator=( const InputField& S );
    
    private:
    
        size_t m_Size;
        char* m_Data;
    };
    
    struct HistoryBuffer
    {
        HistoryBuffer( size_t Size, size_t FieldSize );
    
        bool AddBuf( const InputField& S );
    
        char* GetData( size_t Index ); //Return the Indexth element in terms of age. Returns NULL if Index is 0 or => m_Size
        InputField* At( size_t Index );
    
    private:
    
        size_t m_WriteIndex; //Next item to be written. This-1 = most recent item.
        size_t m_Size;
        xstl::Array<InputField> m_Data;
    };
    
    struct MemoryLog
    {
        MemoryLog();
        ~MemoryLog();
        
        size_t WriteMessage( const xstl::String& Msg ); //Returns line number written.
        xstl::String ReadMessage( size_t L, bool bLineNumbers = false ); //Returns message at line L. If line doesnt exist, returns an empty string. 
        //Make sure to check String.IsEmpty()
        
        size_t GetLines(); //Gets number of actual lines in the log.
        
    private:
        
        xstl::Array<size_t> m_Lines; //Track the byte offset of every new line.
        FILE* m_MemoryFileWrite;
        char* m_BufferLoc; //Memory Log location.
        size_t m_BufferSize; //Memory Log size in bytes.
    };
    
    //Buffer for asynchronous logging. Logging thread will add entries to array which will be read
    //by the actual file-logging thread.
    struct MessageQueue
    {
#define MQ_IDLE_KILL_TIME 100 //How long a message queue is to be idle before cleaning it up.
        
        MessageQueue( size_t BufferSize, std::thread::id Id );
        
        //Write to log buffer, returns false if WriteIndex+1 == ReadIndex, indicating buffer full.
        //If successful, increments m_WriteIndex.
        bool AddMessage( const xstl::String& Msg );
       
        //Check if/how many messages have been dropped due to buffer overflow.
        //Resets counter.
        size_t NumMissed();
       
        //Get m_ReadIndex, if m_ReadIndex == m_WriteIndex, returns NULL, indicating
        //no messages to read. Advances readindex
        xstl::String* GetMessage();
        
        //Get the thread id this queue belongs to.
        std::thread::id GetThreadId();
        
        double idleTime = 0; //How long has this queue been idle? (Resource deallocation)
        std::atomic_bool m_bCanDelete = true; //True if not being read or written to.
        
    private:
        
        size_t m_ReadIndex = 0;
        size_t m_WriteIndex = 0;
        size_t m_BufferSize;
        size_t m_MissedMessages = 0;
        std::thread::id m_QueueId;
        
        xstl::Array<xstl::String> m_Messages;
    };
    
    //====================================================================================
    //  Logging
    //====================================================================================
    
    void write( const xstl::String& Msg, bool bWriteMemory = true, bool bWriteFile = true ); //Write to log files.
    void processLog(); //Process Message Queues, for threaded messages.
    void GetLock(); //Waits for lock to finish. Technically 
    
    //====================================================================================
    //  Input Processing
    //====================================================================================
    
    void command(); //Send InputBuffer to Engine & clear buffer
    void backspace(); 
    void addCharacter( int Ch ); //For typing into input field.
    void arrowRight();
    void arrowLeft();
    void historyUp();
    void historyDown();
    void scrollUp();
    void scrollDown();
    void end(); //m_CurX to end of input field.
    void home(); //m_CurX to start of input field.
    void scrollEnd(); //Set m_LineY to highest possible value (Scroll to end)
    void scrollHome(); //Set m_LineY to lowest possible value (scroll to top)
    
    //====================================================================================
    //  Terminal Rendering
    //====================================================================================
    
    void render(); //Render the terminal.
    
    //====================================================================================
    //  Internal Commands
    //====================================================================================
    
    void processCommand( const xstl::String& Command );
    inline void term_jump( const xstl::Array<xstl::String>& Args ); //( unsigned int x ) - Jump to line x.
    
    //====================================================================================
    //  Utility
    //====================================================================================
    
    InputField* bufferSel( signed int Index ); //Get Input Buffer based on selection index.
    size_t getLWindowSize(); //Get number of lines printable by current window.
    
    //====================================================================================
    
    //Controls
    size_t m_CurX; //Cursor position in input field.
    size_t m_LineY; //Scroll position in log window. Represents the line after the last line visible in the window. (Never less than getLWindowSize())
    signed int m_HSelect;
    bool m_bLastLine; //Is m_LineY at the end?
    
    //Terminal
    u64 m_Lines, m_Columns; //Terminal Dimensions
    
    //Loop control
    bool    m_bUpdate; //Do we need to update the terminal?
    
    //Input
    InputField* m_Field;
    HistoryBuffer* m_HistoryBuffer;
    
    //Logging
    xstl::FileStreamOut m_LogFile;
    bool m_bOpenLog = false; //Is logfile valid?
    MemoryLog  m_MemoryLog;
    
    //Threaded Logging
    u64 m_ChunkSize; //How many messages to unravel from Buffer per loop.
    xstl::Array<MessageQueue*> m_Queues; //List of log-message queues, one per extra thread.
    std::mutex queueLock; //Since log handles calls from different threads seamlessly to the caller, it is nessecary
    //to guard the queue array from race-conditions.
};
