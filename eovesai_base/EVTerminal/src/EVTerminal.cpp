  /*==================================================================================================
 * Eovesai Engine - 2019 Rrajigar Dragoons
 * Terminal.cpp - Terminal Interface
 * 
 * Written by Jesse 'Kyararos' Kowalik
*=================================================================================================*/
//TODO: test tty environment, test threaded logging, implement configuration api. Divider indicators.
#include "EVTerminal.h"

#define QUEUE_SPINRATE 1000 //How often the new queue spinlock should trylock in hz.

//================================================================================================*/ 
// EVTerminal
//================================================================================================*/ 

EVTerminal::EVTerminal( void (*FuncCommandIface)( const xstl::String& ) )
{
    m_FuncCommandIface = FuncCommandIface;
    m_MyThread = std::this_thread::get_id();
}

//================================================================================================*/ 
// InputField
//================================================================================================*/ 

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::InputField::InputField( size_t Size ) : m_Size( Size )
{
    m_Size = Size;
    m_Data = new char[Size];
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::InputField::InputField( const InputField& O ) : m_Size( O.m_Size ),  m_Data( new char[ O.m_Size ] ) {}

EVTerminal::~EVTerminal() {}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::InputField::~InputField()
{
    delete[] m_Data;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

bool EVCursesTerminal::InputField::AddChar( size_t Index, char C )
{
    if( Index>m_Size-2 ) //Reserve last spot for null-terminator.
        return false;
    
    if( m_Data[m_Size-2] != 0 )
        return false;
    
    for( int i = m_Size-2; i>Index; i-- )
    {
        m_Data[i]=m_Data[i-1];
    }
    
    m_Data[Index] = C;
    m_Data[m_Size-1] = 0; //Safety assurance to make sure the last index is always null.
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

bool EVCursesTerminal::InputField::DelChar( size_t Index )
{
    if( Index==0 )
        return false;
    
    if( Index>m_Size-1 )
        return false;
    
    for( int i = Index; i<=m_Size-1; i++ ) 
    {
        m_Data[i-1]=m_Data[i];
    }
    
    m_Data[m_Size-1] = 0; //Safety assurance to make sure the last index is always null.
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::InputField::Clear()
{
    for( int i = 0; i<m_Size; i++ ) 
    {
        m_Data[i]=0;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::InputField::GetEnd() const
{
    for( unsigned int i = 0; i<=m_Size-1; i++ ) 
    {
        if( m_Data[i] == 0 )
            return i;
    }
    
    return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

char* EVCursesTerminal::InputField::GetData()
{
    return m_Data;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::InputField::GetSize() const
{
    return m_Size;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::InputField::operator=( const InputField& S )
{
    for( unsigned int i = 0; ( i<m_Size ) && (i<S.m_Size) ; i++ )
    {
        m_Data[i] = S.m_Data[i];
    }
}

//================================================================================================*/ 
// HistoryBuffer
//================================================================================================*/ 
  
EVCursesTerminal::HistoryBuffer::HistoryBuffer( size_t Size, size_t FieldSize  ) : m_Size( Size ), m_Data( m_Size, InputField( FieldSize ) )
{
    m_WriteIndex = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

bool EVCursesTerminal::HistoryBuffer::AddBuf( const InputField& S )
{
    m_Data[ m_WriteIndex ] = S;
    
    if( m_WriteIndex==m_Size-1 )
    {
        m_WriteIndex = 0;
    }
    else
        m_WriteIndex++;
    
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

char* EVCursesTerminal::HistoryBuffer::GetData( size_t Index )
{
    return At(Index)->GetData();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::InputField* EVCursesTerminal::HistoryBuffer::At( size_t Index )
{
    if( Index > m_Size-1 )
        return NULL;
    
    signed int r = (signed int)Index-(signed int)m_WriteIndex;
    
    if( r>0 )
    {
        return &m_Data[m_Size-r];
    }
    else
    {
        return &m_Data[m_WriteIndex-Index];
    }
    
}

//================================================================================================*/ 
// MemoryLog Interface
//================================================================================================*/ 

EVCursesTerminal::MemoryLog::MemoryLog()
{
    m_MemoryFileWrite = open_memstream( &m_BufferLoc, &m_BufferSize );
    m_Lines.PushBack( 0 );
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::MemoryLog::~MemoryLog()
{
    fclose( m_MemoryFileWrite );
    if( m_BufferSize > 0 )
        free( m_BufferLoc );
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::MemoryLog::WriteMessage( const xstl::String& Msg )
{
    char terminator[1] = "";
    //Add new offset index. Add null character at end to terminate line.
    m_Lines.PushBack( fwrite( Msg.Data(), 1, Msg.Size(), m_MemoryFileWrite ) + 1 + m_Lines.Back() );
    fwrite( terminator, 1, 1, m_MemoryFileWrite ); //Null Terminator
    fflush( m_MemoryFileWrite );
    return m_Lines.Size();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

xstl::String EVCursesTerminal::MemoryLog::ReadMessage( size_t L, bool bLineNumbers )
{
    if( L >= GetLines() )
        return xstl::String(); //Return the empty string.
    
    xstl::String str;
    
    size_t loffset = m_Lines[L];
    
    if( ( m_BufferLoc + loffset ) > (m_BufferLoc + m_BufferSize) )
        return "MemoryLog::ReadMessage error: Access outside of buffer";
    
    str.Assign( m_BufferLoc + loffset );
    
    if( bLineNumbers )
        str.Insert( 0, xstl::String( (unsigned int)L ) + ":" );
    
    return str;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::MemoryLog::GetLines()
{
    if( m_Lines.IsEmpty() )
        return 0;
    
    return m_Lines.Size()-1;
}

//================================================================================================*/ 
// MessageQueue
//================================================================================================*/ 

EVCursesTerminal::MessageQueue::MessageQueue( size_t BufferSize, std::thread::id Id )
{
    m_Messages.Resize( BufferSize );
    m_BufferSize = BufferSize;
    m_QueueId = Id;
}
        
///////////////////////////////////////////////////////////////////////////////////////////////////

bool EVCursesTerminal::MessageQueue::AddMessage( const xstl::String& Msg )
{
            //Check if Buffer is full.
    if( ( m_WriteIndex+1==m_ReadIndex ) ||
            ( m_WriteIndex==m_BufferSize-1 && m_ReadIndex == 0 ) )
    {
        m_MissedMessages++;
        idleTime = 0.0f;
        return false;
    }
           
    m_Messages[m_WriteIndex] = Msg;
            
    if( m_WriteIndex==m_BufferSize-1 )
        m_WriteIndex = 0;
    else
        m_WriteIndex++;
    
    idleTime = 0.0f;
    
    return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::MessageQueue::NumMissed()
{
    size_t missed = m_MissedMessages;
    m_MissedMessages = 0;
    return missed;
}
        
///////////////////////////////////////////////////////////////////////////////////////////////////

xstl::String* EVCursesTerminal::MessageQueue::GetMessage()
{
    if( m_ReadIndex == m_WriteIndex )
        return NULL;
    
    xstl::String* ptr;
    ptr = &(m_Messages[m_ReadIndex]);
    
    if( m_ReadIndex==m_BufferSize-1 )
        m_ReadIndex = 0;
    else
        m_ReadIndex++;
    
    return ptr;
}

std::thread::id EVCursesTerminal::MessageQueue::GetThreadId()
{
    return m_QueueId;
}

//================================================================================================*/ 
// EVTerminal
//================================================================================================*/

EVCursesTerminal::EVCursesTerminal( void (*FuncCommandIface)( const xstl::String& ), size_t FieldSize, 
size_t HistorySize ) : EVTerminal(FuncCommandIface)
{
    initscr();
    noecho();
    
    keypad( stdscr, true );
    nodelay( stdscr, true );
    
    m_Lines = LINES;
    m_Columns = COLS;
    
    m_CurX = 0;
    m_LineY = 0;
    m_HSelect = 0;
    
    m_Field = new InputField( FieldSize );
    m_HistoryBuffer = new HistoryBuffer( HistorySize, FieldSize );
    
    if ( m_LogFile.Open( "./Eovesai.log.txt" ) == 0 ) //TODO:Timestamp log names!
        m_bOpenLog = true;
    
    m_ChunkSize = 8;
    
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::~EVCursesTerminal()
{
    m_LogFile.Close();
    
    endwin();
    
    m_Queues.Clear();
    delete m_Field;
    delete m_HistoryBuffer;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::Loop() //TODO: More graceful approach to detecting non-ascii keys.
{
    int input;
    static auto lastTime = std::chrono::system_clock::now();
    
    //Timers
    const double cleanTime = 10.0f; //How often to garbage-collect threads.
    static double cleanTimer = 0.0f; 
    
    std::chrono::duration<double> deltaTime = std::chrono::system_clock::now() - lastTime;
    lastTime = std::chrono::system_clock::now();
    
    cleanTimer += deltaTime.count();
    
    input = getch();
    
    while( input != ERR )
    {
        switch ( input )
        {
            //LineFeed/Enter
            case 10:
            case 13:
            {
                command();
                break;
            }
            
            //Backspace or Del
            case 8:
            case 127:
            case 263: //Keypad hack
            case 330:
            {
                backspace();
                break;
            }
            
            //Displayable/Typable characters
            case 32 ... 125:
            {
                addCharacter( input );
                break;
            }
            
            //Arrow Right
            case 261:
            {
                arrowRight();
                break;
            }
            
            //Arrow Left
            case 260:
            {
                arrowLeft();
                break;
            }
            
            //Arrow Down
            case 258:
            {
                historyDown();
                break;
            }
                
            //Arrow Up
            case 259:
            {
                historyUp();
                break;
            }
            
            //PageUp
            case 339:
            {
                scrollUp();
                break;
            }
            
            //PageDown
            case 338:
            {
                scrollDown();
                break;
            }
            
            //Home
            case 262:
            {
                home();
                break;
            }
            
            //End
            case 360:
            {
                end();
                break;
            }
            
            case 27: //Escape characters if in xorg.
            {
                input = getch();
                if( input = '[' )
                {
                    input = getch();
                    switch( input )
                    {
                        case 'H':
                            home(); //[H = Home
                            break;
                        case 'F':
                            end(); //[F = End
                            break;
                    }
                }
            }
            
            default:
                break;
            
        }
        input = getch();
    }
    
post:
    
    processLog();
    
    if( cleanTimer >= cleanTime ) //Cleanup idle queues
    {
        for( size_t i = 0; i<m_Queues.Size(); i++ )
        {
            if( m_Queues[i] == NULL )
                continue;
            
            m_Queues[i]->idleTime += cleanTimer;
            if( ( m_Queues[i]->idleTime ) >= MQ_IDLE_KILL_TIME )
            {
                if( queueLock.try_lock()  )
                {
                    if(  m_Queues[i]->m_bCanDelete )
                    {
                        delete m_Queues[i];
                        m_Queues[i] = NULL;
                        queueLock.unlock();
                    }
                }
            }
        }
        cleanTimer = 0.0f;
    }
    
    if( m_bUpdate )
        render();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::Log( const xstl::String& Msg, std::thread::id tid )
{
    bool bMadeMatch = false;
    
    if( m_MyThread == tid )
    {
        write( Msg );
        return;
    }
    else if( m_Queues.Size() > 0 )//For parrallel threads.
    {
        for( int i=0; i<m_Queues.Size(); i++ ) 
        {
            if( m_Queues[i]==NULL )
                continue;
            
            m_Queues[i]->m_bCanDelete = false;
            if( m_Queues[i]->GetThreadId() == tid )
            {
                m_Queues[i]->AddMessage( Msg );
                bMadeMatch = true;
                m_Queues[i]->m_bCanDelete = true;
                break;
            }
            m_Queues[i]->m_bCanDelete = true;
        }
    }
    
    if( bMadeMatch == true ) //If a queue for our thread does not exist, we must create a new queue.
        return;
    
    //For parrallel threads. It is imperative that the following code segment be reliable or else it will result in threadlock.
trylock:
    if( queueLock.try_lock() )
    {
        if( m_Queues.Size() > 0 ) //Find existing spot in array if available first, to avoid memory leak.
        {
            for( int i=0; i<m_Queues.Size(); i++ ) 
            {
                if( m_Queues[i] == NULL )
                {
                    m_Queues[i] = new MessageQueue( 256, tid );
                    m_Queues[i]->AddMessage( Msg );
                    bMadeMatch = true;
                    break;
                }
            }
        }
        
        if( !bMadeMatch )
        {
            m_Queues.PushBack( new MessageQueue( 256, tid ) );
            m_Queues.Back()->AddMessage( Msg );
        }
        
        queueLock.unlock();
    }
    else
    {
        usleep( 1000000/QUEUE_SPINRATE );
        goto trylock;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::write( const xstl::String& Msg, bool bWriteMemory, bool bWriteFile )
{
    if( m_bOpenLog )
    {
        m_LogFile.Write( ( Msg + '\n' ).Data(), Msg.Size()+1 );
        //TODO: Flush contents to write to disk.
    }
    
    m_MemoryLog.WriteMessage( Msg + '\n' );
    
    if( m_LineY+1 == m_MemoryLog.GetLines()-1 )
        m_LineY++;
    
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::processLog()
{
    size_t numMissed = 0;
    xstl::String* msg;
    bool bNewMessage = false;
    u32 missed;
    
    for( int i=0; i<m_Queues.Size(); i++ )
    {
        if( m_Queues[i] == NULL )
            continue;
        
        for( int i2=0; i2<m_ChunkSize; i2++ )
        {
            missed = m_Queues[i]->NumMissed();
            if( missed > 0 )
            {
                write( "[EVCursesTerminal::processLog]: Error: Thread Queue[" + xstl::String(i) + "] Missed Messages: " + xstl::String(missed) + "." );
            }
            msg = m_Queues[i]->GetMessage();
            if( msg == NULL )
                break;
            else
            {
                write( *msg );
                bNewMessage = true;
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::Resize( int sig ) //TODO: Fix resize behavior, segfault
{
    /*
    endwin();
    refresh();
    
    m_Lines = LINES;
    m_Columns = COLS;
    
    m_bUpdate = true;
    */
    return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::Cont( int sig )
{
    //Resize( sig ); //Just incase terminal size changed since resume.
    return;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::command()
{
    xstl::String buffer;
    
    buffer.Assign( bufferSel( m_HSelect )->GetData() );
    
    if( buffer[0] == 0 )
        return; //Stop if empty.
    
    m_HistoryBuffer->AddBuf( *bufferSel( m_HSelect ) );
    
    xstl::String command = buffer.Substr( 0, 4 );
    if(  command == "term" ) //Terminal command
    {
        try
        {
            processCommand( buffer.Substr( 5 ) );
        }
        catch( const std::out_of_range& e )
        {
        }
    }
    else
    {
        //Print Command in Log 
        write( "> " + buffer );
        m_FuncCommandIface( buffer );
    }
    
    //clear buffer
    m_Field->Clear();
    
    m_CurX = 0;
    m_HSelect = 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::backspace()
{
    if( m_HSelect != 0 )
    {
        *m_Field = *bufferSel( m_HSelect );
        m_HSelect = 0;
    }
    
    if( m_Field->DelChar( m_CurX ) )
    {
        m_CurX--;
        m_bUpdate = true;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::addCharacter( int Ch )
{
    if( m_HSelect != 0 )
    {
        *m_Field = *bufferSel( m_HSelect );
        m_HSelect = 0;
    }
    
    if( m_Field->AddChar( m_CurX, Ch ) ) //Note: Ch > 255 unchecked
    {
        m_CurX++;
        m_bUpdate = true;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::arrowRight()
{
    if( m_CurX>=bufferSel( m_HSelect )->GetSize()-1 )
        return;
    
    if( bufferSel( m_HSelect )->GetData()[m_CurX] == 0 )
        return;
    
    m_CurX++;
    
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::arrowLeft()
{
    //Do nothing at start of buffer.
    if( m_CurX == 0 )
        return;
    
    m_CurX--;
    
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Step through history buffer (older)
void EVCursesTerminal::historyUp()
{
    if( bufferSel( m_HSelect+1 ) == 0 )
        return;
    
    m_HSelect++;
    m_CurX = bufferSel( m_HSelect )->GetEnd();
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//Step through history buffer (more recent)
void EVCursesTerminal::historyDown()
{
    if( m_HSelect==0 )
        return;
    
    m_HSelect--;
    m_CurX = bufferSel( m_HSelect )->GetEnd();
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::scrollUp()
{
    if( m_LineY == 0 )
        return;
    
    m_LineY--;
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::scrollDown()
{
    if( m_LineY+1 >= m_MemoryLog.GetLines() )
        return;
    
    m_LineY++;
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::end()
{
    m_CurX = bufferSel( m_HSelect )->GetEnd();
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::home()
{
    m_CurX = 0;
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::scrollEnd()
{
    m_LineY = m_MemoryLog.GetLines()-1;
    
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::scrollHome()
{
    m_LineY = 0;
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::render()
{
    m_bUpdate = false; //Set first incase signal interrrupt happens during function call. - Rerender.
    
    clear();
    
    const size_t scrY = getLWindowSize();
    const size_t scrA = m_Columns * getLWindowSize(); //Buffer size in bytes, max possible screensize.
    const size_t memLines = m_MemoryLog.GetLines(); //Number of lines available in buffer to be rendered.
    
    size_t numLines = 0; //How much rendered so far.
    size_t numColumns = 0;
    xstl::String str;
    xstl::String scrBuffer( scrA+1, 0 );
    size_t scrIndex = scrBuffer.Size()-1;
    
    if( true )
    {
        //build window buffer.
        for( size_t i = 0; i<=memLines; i++ ) //For each message in memory buffer.
        {
            str = m_MemoryLog.ReadMessage( m_LineY-i );
            
            if( str.IsEmpty() )
                break;
        
            for( size_t i2 = str.Size(); i2>0; i2-- ) //Render the string, keeping track of screen dimensions.
            {
                if( scrIndex == 0 ) //Safeguard to prevent outside access.
                    break;
                
                if( ( numColumns >= m_Columns ) || ( str[i2-1] == '\n' ) )
                {
                    if( numLines >= scrY ) //Ran out of screenspace, stop.
                        break;
                    
                    scrIndex--;
                    numLines++;
                    numColumns = 0;
                }
                
                scrBuffer[scrIndex] = str[i2-1];
                scrIndex--;
                numColumns++;
            }
            
            if( scrIndex == 0 ) //Safeguard to prevent outside access.
                    break;
            
            if( numLines>=scrY ) //Ran out of lines, stop.
                break;
        }
        
        for( ; numLines<scrY; numLines++ ) //Fill remaining lines with newlines.
        {
            scrBuffer[scrIndex] = '\n';
            scrIndex--;
        }
        
        //Render buffer.
        numLines = 0;
        numColumns = 0;
        
        for( size_t i = scrIndex; i<scrBuffer.Size(); i++ )
        {
            if( scrBuffer[i] ==  0 ) //ignore null characters.
                continue;
            
            if( ( numColumns >= m_Columns ) || ( scrBuffer[i] == '\n' ) )
            {
                if( numLines == scrY ) //Ran out of screenspace, stop.
                    break;
                
                numLines++;
                numColumns = 0;
            }
                
            addch( scrBuffer[i] );
            numColumns++;
        }
    }
    
    Divider:
    //Render Divider
    
    if( ( m_LineY != m_MemoryLog.GetLines()-1 ) && ( m_MemoryLog.GetLines() != 0 ) ) //Indicator for if at end of log.
        addch( 'V' );
    else
        addch( '-' );
    
    for( int i=1; i<m_Columns-2; i++ )
    {
        if( ( i == m_Columns-3 ) && ( m_LineY != m_MemoryLog.GetLines()-1 ) && ( m_MemoryLog.GetLines() != 0 ) )
            addch( 'V' );
        else
            addch( '-' );
    }
    
    addch( '\n' );
    
    //Render Input Field
    
    char fieldchar[] = { '>', ' ', '\0' };
    
    addstr( fieldchar );
    addstr( bufferSel( m_HSelect )->GetData() );
    
    //Render Cursor
    int x = (m_CurX+2) % m_Columns;
    int y = (m_Lines-1) - __DEFAULTFIELDY;
    
    y += (m_CurX+2) / m_Columns;
    
    move( y, x ); 
    
    refresh();
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::processCommand( const xstl::String& Args ) //Extract command from 'term' arguments, siphon arguments from that.
{
    if( Args.Size() == 0 ) //Term called without arguments.
    {
        return;
    }
    
    xstl::String command;
    size_t commandEnd = 0; //The last index of the command str.
    xstl::Array<xstl::String> commandArgs;
    
    //Extract comamnd
    for( size_t i = 0; i<Args.Size(); i++ )
    {
        if( Args[i] == 0x20 ) //If we hit a space, then the command has ended.
        {
            commandEnd = i;
            break;
        }
        
        command.PushBack( Args[i] );
    }
    
    if( command.IsEmpty() )
    {
        write( "[EVCursesTerminal::term]: Syntax Error (command empty)." );
        return;
    }
    
    if( Args[commandEnd+1] == 0x20 ) //Doublespace after command.
    {
        write( "[EVCursesTerminal::term]: Syntax Error (doublespace)." );
        return;
    }
    
    size_t last = commandEnd+1;
    //Extract arguments.
    for( size_t i = last; i<Args.Size(); i++ ) //+1 to skip the space.
    {
        if( Args[i] == 0x20 )
        {
            try
            {
                if( Args[i+1] == 0x20 )
                {
                    write( "[EVCursesTerminal::term]: Syntax Error (doublespace)." );
                    return;
                }
            }
            catch( const std::out_of_range& e ) //TODO: Implementation-independent catch.
            {
                write( "[EVCursesTerminal::term]: Syntax Error (trailing space)." );
                return;
            }
            if( Args[i+1] == 0 )
            {
                write( "[EVCursesTerminal::term]: Syntax Error (trailing space)." );
                return;
            }
            
            commandArgs.PushBack( Args.Substr( last, i-last ) );
            i++;
            last = i;
            continue;
        }
        
        if( i == Args.Size()-1 ) //If we hit the end of the Arguments.
        {
            commandArgs.PushBack( Args.Substr( last, i+1-last ) );
            break;
        }
            
    }
    
    if( command == "jump" )
    {
        term_jump( commandArgs );
    }
    else
    {
        write( "[EVCursesTerminal::term]: Unknown command: '" + command + "'" );
        return;
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////

void EVCursesTerminal::term_jump( const xstl::Array<xstl::String>& Args )
{
    size_t index = 0;
    size_t mult = 1;
    size_t size = Args.Size();
    
    //Process syntax.
    if( Args.IsEmpty() )
    {
        write( "[EVCursesTerminal::term_jump()]: Syntax error, Missing Argument(s). format: term jump <uint>." );
        return;
    }
    
    if( size > 1 )
    {
        write( "[EVCursesTerminal::term_jump()]: Syntax error, too many arguments. format: term jump <uint>." );
        return;
    }
    
    for( size_t i = 0; i<Args[0].Size(); i++ )
    {
        if( ( Args[0][i] < 48 ) || ( Args[0][i] > 57 ) )
        {
            write( "[EVCursesTerminal::term_jump()]: Syntax error, invalid argument(s) format: term jump <uint>." );
            return;
        }
    }
    
    //Convert ascii string numbers to int.
    for( size_t i = 0; i<Args[0].Size(); i++ ) 
    {
        index += (Args[0][ Args[0].Size()-(i+1) ]-48) * mult; //-48 to shift ascii value.
        mult *= 10;
    }
    
    if( index >= m_MemoryLog.GetLines() )
        index = m_MemoryLog.GetLines()-1;
    
    m_LineY = index;
    m_bUpdate = true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

EVCursesTerminal::InputField* EVCursesTerminal::bufferSel( signed int Index )
{
    if( Index == 0 )
        return m_Field;
    else
        return m_HistoryBuffer->At( Index );
}

///////////////////////////////////////////////////////////////////////////////////////////////////

size_t EVCursesTerminal::getLWindowSize()
{
    return m_Lines-__DEFAULTFIELDY-__DEFAULTDIVY-1;
}
 

