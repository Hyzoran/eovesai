/*==================================================================================================
 * Eovesai Engine - 2019 Rrajigar Dragoons
 * Eovesai.cpp - Executable
 * 
 * Written by Jesse 'Kyararos' Kowalik
*=================================================================================================*/
#include <functional>
#include <unistd.h>

#include <libxstl/XTypes.h>

#include "EVTerminal/EVTerminal.h"
#include "EVCore/EVCore.h"

#define DEFAULTFIELDSIZE 256
#define DEFAULTHISTORYSIZE 32
#define DEFAULT_TERM_TICKRATE 500

EVTerminal* g_TerminalDevice;

void RedirectLog( const xstl::String& Str ) //Redirect log output to terminal device.
{
    static thread_local std::thread::id tid = std::this_thread::get_id();
    
    g_TerminalDevice->Log( Str, tid );
}

void handle_SIGWINCH( int signal )
{
    if( g_TerminalDevice!=NULL )
        g_TerminalDevice->Resize( signal );
}

void handle_SIGCONT( int signal )
{
    if( g_TerminalDevice!=NULL )
        g_TerminalDevice->Cont( signal );
}

int main( int argc, char* argv[] )
{
    g_TerminalDevice = new EVCursesTerminal( EVCore::ConsoleCommand, DEFAULTFIELDSIZE, 
        DEFAULTHISTORYSIZE );
    
    EVCore::Init( &RedirectLog );
    
    std::signal( SIGWINCH, handle_SIGWINCH );
    std::signal( SIGCONT, handle_SIGCONT );
    
    while( EVCore::Status() != -1 )
    {
        g_TerminalDevice->Loop();
        usleep( 1000000/DEFAULT_TERM_TICKRATE );
    }
    
    delete g_TerminalDevice;
    
    return 0;
}  
