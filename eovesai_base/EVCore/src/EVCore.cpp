  /*==================================================================================================
 * Eovesai Engine - 2019 Rrajigar Dragoons
 * Core.cpp
 * 
 * Written by Jesse 'Kyararos' Kowalik
*=================================================================================================*/   
#include <unistd.h>

#include "EVCore.h"

//====================================================================================
// Argument
//====================================================================================

EVCore::Argument::Argument( const xstl::String& Str ) : m_Data(Str), m_Type(StringType(Str))
{
}

//====================================================================================

u64 EVCore::Argument::StringProcess_u64( const xstl::String& Str )
{
    u64 value = 0;
    u64 mult = 1;
    size_t size = Str.Size();
    
    //Checking for out of range values. u64 capped at 17,999,999,999,999,999,999 for simplicity
    if( size == 20 )
    {
        if( Str[0] == 1+48 )
        {
            if( Str[1] > 7+48 )
            {
                Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type u64 ( > 17,999P." );
                return 0; 
            }
        }
        else if ( Str[0] > 1+48 )
        {
            Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type u64 ( > 17,999P." );
            return 0; 
        }
    }
    else if( size > 20 )
    {
        if( size == 21 )
        {
            if( !( Str[0] == '+' )  )
            {
                Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type i64 ( > 9,199P or invalid String format." );
                return 0; 
            }
        }
        else
        {
            Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type i64 ( > 9,199P or invalid String format." );
            return 0; 
        }
    }
    
    for( size_t i = 1; i<=size; i++ )
    {
        if ( ( Str[size-i] < 48 ) || ( Str[size-i] > 57 ) )
        {
            Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type u64." );
            return 0;
        }
        else
        {
            value += ( Str[size-i] - 48 ) * mult;
            mult *= 10;
        }
    }
        
    return value;
}

//====================================================================================

i64 EVCore::Argument::StringProcess_i64( const xstl::String& Str )
{
    i64 value = 0;
    i64 mult = 1;
    size_t size = Str.Size();
    bool bNegative = false;
    
    //Checking for out of range values. i64 capped at +-9,199,999,999,999,999,999 for simplicity
    if( size == 19 )
    {
        if( Str[0] == 9+48 )
        {
            if( Str[1] >= 2+48 )
            {
                Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type i64 ( > 9,199P." );
                return 0; 
            }
        }
    }
    else if( size > 19 )
    {
        if( size == 20 )
        {
            if( !( ( Str[0] == '-' ) || ( Str[0] == '+' ) ) )
            {
                Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type i64 ( > 9,199P or invalid String format." );
                return 0; 
            }
        }
        else
        {
            Log( "EVCore::StringProcess: ERROR: Out of range String conversion for type i64 ( > 9,199P or invalid String format." );
            return 0; 
        }
    }
    
    for( size_t i = 1; i<=size; i++ )
    {
        if( Str[size-i] == '-' )
        {
            if( i == size )
                bNegative = true;
            else
            {
                Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type i64." );
                return 0;
            }
        }
        else if( Str[size-i] == '+' )
        {
            if( i == size )
                bNegative = false;
            else
            {
                Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type i64." );
                return 0;
            }
        }
        else if ( ( Str[size-i] < 48 ) || ( Str[size-i] > 57 ) )
        {
            Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type i64." );
            return 0;
        }
        else
        {
            value += ( Str[size-i] - 48 ) * mult;
            mult *= 10;
        }
    }
        
    if( bNegative )
        value *= -1;
        
    return value;
}

//====================================================================================

double EVCore::Argument::StringProcess_double( const xstl::String& Str )
{
    double value = 0;
    double mult = 1;
    size_t size = Str.Size();
    size_t decimalIndex = 0;
    bool bDecimal = false;
    bool bNegative = false;
    
   for( size_t i = 1; i<=size; i++ )
    {
        if( Str[size-i] == '-' )
        {
            if( i == size )
                bNegative = true;
            else
            {
                Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type double." );
                return 0;
            }
        }
        else if( Str[size-i] == '+' )
        {
            if( i == size )
                bNegative = false;
            else
            {
                Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type double." );
                return 0;
            }
        }
        else if( Str[size-i] == '.' )
        {
            if( !bDecimal )
            {
                decimalIndex = i;
                bDecimal = true;
            }
            else
            {
                Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type double." );
                return 0;
            }
        }
        else if ( ( Str[size-i] < 48 ) || ( Str[size-i] > 57 ) )
        {
            Log( "EVCore::StringProcess: ERROR: Conversion attempted on invalid String syntax for type double." );
            return 0;
        }
        else
        {
            value += ( Str[size-i] - 48 ) * mult;
            mult *= 10;
        }
    }
        
    if( ( value == 1.0 / 0.0 ) || ( value == -1.0 / 0.0 ) ) //Check for NaN
    {
        Log( "EVCore::StringProcess: ERROR: Conversion attempted on out of range double." );
        return 0;
    }
    
    if( bNegative )
        value *= -1;
        
    if( decimalIndex > 0 )
        value /= 10 * decimalIndex;
        
    return value;
}

//====================================================================================

xstl::Array<EVCore::Argument> EVCore::Argument::SegmentArguments( const xstl::String& Str )
{
    bool bQuotes = false;
    
    xstl::Array<EVCore::Argument> SegmentedArguments;
    
    if( Str.IsEmpty() )
        return SegmentedArguments;
    
    size_t last = 0; //Start of next argument
    
    for( size_t i = 0; i<Str.Size(); i++ ) //Quotes and spaces start new argument.
    {
        if( !bQuotes )
        {
            if( i == Str.Size()-1 )
            {
                if( (i-last) != 0 )
                {
                    if( Str[i] == '"' || Str[i] == 0x20 )
                        SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, (i-last) ))); //To avoid trailing delimiter
                    else
                        SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, (i-last) + 1 ))); //+1 to be inclusive of non-space character.
                }     
            }
            else if( Str[i] == 0x20 )
            {
                if( i == last ) //Skip duplicate spaces
                {
                    last++;
                    continue;
                }
                if( (i-last) != 0 )
                    SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, i-last )));
                
                last = i+1;
            }
            else if( Str[i] == '"' )
            {
                if( (i-last) != 0 )
                    SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, i-last )));
                last = i+1;
                bQuotes = !bQuotes;
            }
        }
        else
        {
            if( i == Str.Size()-1 )
            {
                if( (i-last) != 0 )
                {
                    if( Str[i] == '"' || Str[i] == 0x20 )
                        SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, (i-last) ))); //To avoid trailing delimiter
                    else
                        SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, (i-last) + 1 ))); //+1 to be inclusive of non-space character.
                }       
            }
            else
            {
                if( Str[i] != '"' )
                    continue;
                else
                {
                    if( (i-last) != 0 )
                        SegmentedArguments.PushBack(EVCore::Argument(Str.Substr( last, i-last )));
                    
                    last = i+1;
                    bQuotes = !bQuotes;
                }
            }
        }
    }
    
    if(  SegmentedArguments.Size() == 0  ) //Quick fix to oversight with checking against Str.Size()-1.
    {
        if( Str[0] != 0x20 && Str[0] != '"')
            SegmentedArguments.PushBack( EVCore::Argument( Str ) );
    }
    
    return SegmentedArguments;
}

//====================================================================================

EVCore::E_StringType EVCore::Argument::StringType( const xstl::String& Str )
{
    size_t size = Str.Size();
    bool bNegative = false;
    bool bPositive = false;
    bool bDecimal = false;
    
    if( Str.IsEmpty() )
        return E_StringType_void;
    
    for( size_t i = 0; i<size; i++ )
    {
        char c = Str[(size-1)-i];
        
        if( ( c < 48 ) || ( c > 57 ) ) //
        {
            if( c == '.' )
            {
                if ( bDecimal )
                    return E_StringType_String;
                else
                    bDecimal = true;
            }
            else if( ( c == '-' ) || ( c == '+' ) )
            {
                if( bNegative || bPositive )
                    return E_StringType_String;
                else if( i == size-1 )
                {
                    if( c == '-' )
                        bNegative = true;
                    else if( c == '+' )
                        bPositive = true;
                }
                else
                   return E_StringType_String; 
            }
            else
                return E_StringType_String;
        }
    }
    
    if( !bDecimal && !bNegative ) //u64
    {
        if( size == 20 )
        {
            if( Str[0] == 1+48 )
            {
                if( Str[1] > 7+48 )
                {
                    return E_StringType_String;
                }
            }
            else if ( Str[0] > 1+48 )
            {
                return E_StringType_String;
            }
        }
        else if( size > 20 )
        {
            if( size == 21 )
            {
                if( !( Str[0] == '+' ) )
                {
                    return E_StringType_String;
                }
            }
            else
            {
                return E_StringType_String;
            }
        }
        else
            return E_StringType_u64;
    }
    else if( !bDecimal ) //i64
    {
        if( size == 19 )
        {
            if( Str[0] == 9+48 )
            {
                if( Str[1] >= 2+48 )
                {
                    return E_StringType_String;
                }
            }
        }
        else if( size > 19 )
        {
            if( size == 20 )
            {
                if( !( ( Str[0] == '-' ) || ( Str[0] == '+' ) ) )
                {
                    return E_StringType_String; 
                }
            }
            else
            {
                return E_StringType_String;
            }
        }
        else
            return E_StringType_i64;
    }
    else
        return E_StringType_double;
        
}
    
//====================================================================================

time_t EVCore::GetTime()
{
    return time( NULL );
}

//====================================================================================

xstl::String EVCore::GetTimeStr()
{
    return FormatTime( time( NULL ) );
}

//====================================================================================

xstl::String EVCore::FormatTime( const time_t& TimeStamp )
{
    //TODO: Support for different stamp formats. for now just default:
    // YYYY::MM::DD::HH::MM::SS with localtime
    
    tm tm = *localtime( &TimeStamp );
    
    int year, month, day, hour, minute, seconds;
    xstl::String yearstr, monthstr, daystr, hourstr, minutestr, secondsstr;
    
    year = tm.tm_year + 1900;
    month = tm.tm_mon+1;
    day = tm.tm_mday;
    hour = tm.tm_hour;
    minute = tm.tm_min;
    seconds = tm.tm_sec;
    
    yearstr = xstl::String( year );
    monthstr = ( month < 10 ) ? "0" + xstl::String( month ) : xstl::String( month );
    daystr = ( day < 10 ) ? "0" + xstl::String( day ) : xstl::String( day );
    hourstr = ( hour < 10 ) ? "0" + xstl::String( hour ) : xstl::String( hour );
    minutestr = ( minute < 10 ) ? "0" + xstl::String( minute ) : xstl::String( minute );
    secondsstr = ( seconds < 10 ) ? "0" + xstl::String( seconds ) : xstl::String( seconds );
    
    return yearstr + ":" + monthstr + ":" + daystr + ":" + hourstr + ":" + minutestr + ":" + secondsstr;
}

//====================================================================================
// EVCore
//====================================================================================

bool EVCore::Init( void (*FuncLogOut)( const xstl::String& ) )
{
    m_FuncLogOut = FuncLogOut;
}

//====================================================================================

bool EVCore::Deinit()
{
    Log( "<NOTICE>: Core shutting down..." );
    m_Status = -1;
}

//====================================================================================

void EVCore::Crash( xstl::String Msg )
{
    Msg.Insert( 0, "<CRITICAL>: " );
    Log( Msg );
    Deinit();
}

//====================================================================================

i64 EVCore::Status()
{
    return m_Status;
}

//====================================================================================

void EVCore::Log( const xstl::String& Msg, const xstl::String& SystemName, i64 Id )
{
    xstl::String out;
    
    out.Assign( "|" + GetTimeStr() + "|[" + SystemName + ":" + xstl::String( Id ) + "]> " );
    m_FuncLogOut( out + Msg );
}

//====================================================================================

void EVCore::ConsoleCommand( const xstl::String& CommandStr )
{
    xstl::String command;
    size_t commandEnd = 0;
    
    
    if( CommandStr.IsEmpty() )
        return;
    
    if( CommandStr[0] == 0x20 || CommandStr[0] == 0 )
        return;
    
    //Get first 'word' of CommandStr
    for( int i=0; i<CommandStr.Size(); i++ )
    {
        if( CommandStr[i] == 0x20  )
        {
            break;
        }
        
        commandEnd++;
    }
    
    command = CommandStr.Substr( 0, commandEnd );
    
    xstl::Array<Argument> commandArgs;
    
    //Extract arguments, if present.
    if( commandEnd != 0 && commandEnd < CommandStr.Size()-1 )
    {
        commandArgs = Argument::SegmentArguments( CommandStr.Substr( commandEnd+1, CommandStr.Size()-commandEnd ) ); //+1 skip first space.
    }
    
    //Process Command
    
    //=========================================================
    // quit/exit
    
    if( ( command == "quit" ) || ( command == "exit" ) )
    {
       shutdown( commandArgs );
    }
    
    //=========================================================
    // crash
        
    else if( command == "crash" )
    {
        crash( commandArgs );
    }
    
    //=========================================================
    // testmsg
        
    else if( command == "testmsg" )
    {
        testmsg( commandArgs );
    }
    
    //=========================================================
    // threadtest
        
    else if( command == "threadtest" )
    {
        threadtest( commandArgs );
    }
    
    //=========================================================
    // printtestthreads
        
    else if( command == "printtestthreads" )
    {
        printtestthreads( commandArgs );
    }
    
    //=========================================================
    // threadtest
        
    else if( command == "killtestthreads" )
    {
        killtestthreads( commandArgs );
    }
    
    //=========================================================
    // unrecognized
    
    else
    {
        Log("(ConsoleCommand): Invalid Command: '" + command + "'.");
    }
}

//====================================================================================

bool EVCore::CheckArgs( const xstl::String& CommandStr, const xstl::Array<Argument>& Args, const i64 Min, const i64 Max,
                const xstl::Array<E_StringType>& Types )
{
    size_t size = Args.Size();
    xstl::String types[5] = { "void", "u64", "i64", 
                            "double", "String" }; //From E_StringType
    xstl::String syntax;
    
    if( Max < -1 )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Error, malformed CheckArgs call. Max < -1.");
        return false;
    }
    if( Min < 0 )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Error, malformed CheckArgs call. Min < 0.");
        return false;
    }
    if( ( Max < Min ) && ( Max != -1 ) )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Error, malformed CheckArgs call. Max < Min.");
        return false;
    }
    if( Types.IsEmpty() )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Error, malformed CheckArgs call. Empty Types array.");
        return false;
    }
    if( Max != -1 )
    {        
        if( ( Max == 0 ) && ( Types[0] == E_StringType_void ) )
        {}
        else if( Types.Size() > Max )
        {
            Log("(ConsoleCommand:" + CommandStr + "): Error, malformed CheckArgs call. Types > Max");
            return false;
        }   
    }
    
    //Generate expected syntax from argument list.
    for( size_t i = 0; i<Types.Size(); i++ )
    {
        if( i == 0 )
            syntax += "<" + types[Types[i]] + ">";
        else
            syntax += ", <" + types[Types[i]] + ">";
    }
    if( Max == -1 )
        syntax += "... Min: " + xstl::String( Min );
    else if( ( Max > Types.Size() || ( Types.IsEmpty() && ( Max != 0 ) ) ) )
        syntax += "... Min: " + xstl::String( Min ) + " Max: " + xstl::String( Max );
    else
        syntax += " Min: " + xstl::String( Min ) + " Max: " + xstl::String( Max );
    
    //Check if Arguments match up to call.
    if( size < Min )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Too few arguments, expected: " + syntax + ". Given: " + xstl::String((u64)size) + "." );
        return false;
    }
    else if( size > Max )
    {
        Log("(ConsoleCommand:" + CommandStr + "): Too many arguments, expected: " + syntax + ". Given: " + xstl::String((u64)size) + "." );
        return false;
    }
    
    //Check Argument types
    for( size_t i = 0; i<size; i++ )
    {
        E_StringType t;
        
        if( i > ( Types.Size()-1 ) ) //If argument is past the types list, then use last type in that list.
            t = Types[Types.Size()-1];
        else
            t = Types[i];
        
        if( t == E_StringType_String ) //Anything can be converted to string.
            continue;
        else if( t == E_StringType_double ) //Anything except String can be converted to double.
        {
            if( Argument::StringType( Args[i] ) >= E_StringType_String )
            {
                Log("(ConsoleCommand:" + CommandStr + "): Invalid syntax, expected: " + syntax + ". Given: " + GetArgumentTypeList(Args) );
                return false;
            }
        }
        else if( t == E_StringType_i64 ) //Non-integer types cannot be converted to i64
        {
            if( Argument::StringType( Args[i] ) >= E_StringType_double )
            {
                Log("(ConsoleCommand:" + CommandStr + "): Invalid syntax, expected: " + syntax + ". Given: " + GetArgumentTypeList(Args) );
                return false;
            }
        }
        else if( t == E_StringType_u64 ) //Signed types cannot be converted to u64
        {
            if( Argument::StringType( Args[i] ) >= E_StringType_double )
            {
                Log("(ConsoleCommand:" + CommandStr + "): Invalid syntax, expected: " + syntax + ". Given: " + GetArgumentTypeList(Args) );
                return false;
            }
        }
    }
    
    return true;
}

//====================================================================================

xstl::String EVCore::GetArgumentTypeList( const xstl::Array<Argument>& Args )
{
    xstl::String r;
    
    for( size_t i = 0; i<Args.Size(); i++ )
    {
        switch( Args[i].m_Type )
        {
            case E_StringType_void:
            {
                r += "<void>";
                break;
            }
            
            case E_StringType_u64:
            {
                r += "<u64>";
                break;
            }
            
            case E_StringType_i64:
            {
                r += "<i64>";
                break;
            }
            
            case E_StringType_double:
            {
                r += "<double>";
                break;
            }
            
            case E_StringType_String:
            {
                r += "<string>";
                break;
            }
            
            default:
            {
                r += "<void>";
                break;
            }
        }
        if( i != Args.Size()-1 ) 
            r += ", ";
    }
    
    return r;
}

//====================================================================================

void EVCore::shutdown( const xstl::Array<Argument>& Args )
{
    if( CheckArgs( "shutdown", Args, 0, 0 ) )
        Deinit();
}

//====================================================================================

void EVCore::crash( const xstl::Array<Argument>& Args )
{
    if( !CheckArgs( "crash", Args, 0, 1, { E_StringType_String } ) )
        return;
    
    if( Args.IsEmpty() )
        Crash( "Terminal-issued Crash!" );
    else
        Crash( Args[0] );
}

//====================================================================================

void EVCore::testmsg( const xstl::Array<Argument>& Args )
{
    if( !CheckArgs( "testmsg", Args, 0, 1, { E_StringType_String } ) )
        return;
    
    if( Args.IsEmpty() )
        Log( "<NOTICE>: Terminal-issued test message." );
    else
        Log( Args[0] );
}

//====================================================================================

void EVCore::threadtest( const xstl::Array<Argument>& Args )
{
    if( !CheckArgs( "threadtest", Args, 0, 1, { E_StringType_u64 } ) )
        return;
        
    if( Args.Size() == 0 )
    {
        TestThread::m_TestThreads.PushBack( new TestThread( TestThread::m_TestThreads.Size() ) );
        Log( "New TestThread, Id: " + xstl::String( u64(TestThread::m_TestThreads.Size()-1) ) + "." );
        return;
    }
    
    for( size_t i = 0; i<Argument::StringProcess_u64(Args[0]); i++ )
    {
        TestThread::m_TestThreads.PushBack( new TestThread( i ) );
        Log( "New TestThread, Id: " + xstl::String( u64(TestThread::m_TestThreads.Size()-1) ) + "." );
    }
}

//====================================================================================

void EVCore::printtestthreads( const xstl::Array<Argument>& Args )
{
    if( !CheckArgs( "printtestthreads", Args, 0, 0, { E_StringType_void } ) )
        return;
        
    if( TestThread::m_TestThreads.IsEmpty() )
        Log( "Test Threads: 0." );
    else
        Log( "Test Threads: " + xstl::String( u64( TestThread::m_TestThreads.Size() ) ) + ", [0-" + 
            xstl::String( u64( TestThread::m_TestThreads.Size()-1 ) ) + "]." );
}

//====================================================================================

void EVCore::killtestthreads( const xstl::Array<Argument>& Args )
{
    if( !CheckArgs( "killtestthreads", Args, 0, 0, { E_StringType_void } ) )
        return;
        
    size_t count = Args.Size();
    
    count = TestThread::m_TestThreads.Size();
        
    for( size_t i = 0; i<count; i++ )
    {
        if( TestThread::m_TestThreads[i] == NULL )
            continue;
        
        TestThread::m_TestThreads[i]->m_bKillMe = true;
        TestThread::m_TestThreads[i]->m_Handle->detach(); //So that it destroys itself after finishing.
        TestThread::m_TestThreads[i] = NULL;
    }
}

//====================================================================================
xstl::Array<EVCore::TestThread*> EVCore::TestThread::m_TestThreads;

EVCore::TestThread::TestThread( u64 index )
{
    m_Handle = new std::thread( &EVCore::TestThread::SPAMBOI, this );
    id = index;
}

//====================================================================================

void EVCore::TestThread::SPAMBOI()
{
    usleep( waitTimeMs );
    
    xstl::String randstr;
    srand (time(NULL));
    
    while( m_bKillMe == false )
    {
        randstr.Resize( rand() % 240 + 8 );
        
        for( size_t i = 0; i<randstr.Size(); i++ )
        {
            randstr[i] = '0' + rand()%43; //Generate random ascii value between 48-90.
        }
        
        Log( "ThreadTest: " + randstr );
        
        if( waitTimeMs <= 100 )
            waitTimeMs = 100;
        else
            waitTimeMs *= 0.75l;
        
        usleep( waitTimeMs * 1000 );
    }
    
    Log( "Deleted TestThread, Id: " + xstl::String( u64(id) ) + "." );
    delete this;
}
