 /*==================================================================================================
 * Eovesai Engine - 2019 Rrajigar Dragoons
 * Core.h - Core System
 * 
 *  Engine Core. The interface that manages Machine components and Environment Properties.
 * 
 * Written by Jesse 'Kyararos' Kowalik
*=================================================================================================*/  
#pragma once

#include <ctime>
#include <mutex>
#include <functional>
#include <thread>

#include <libxstl/XTypes.h>
#include <libxstl/XArray.h>
#include <libxstl/XString.h>

namespace EVCore
{
    //====================================================================================
    //  Data
    //====================================================================================
    
    //StringType Enum, for determining what types a string is "compatible" with.
    //In order from most primative to most advanced. A lower value should always be convertible to a higher one.
    enum E_StringType
    {
        E_StringType_void = 0, //Invalid or empty String, or non-applicable characters present.
        E_StringType_u64 = 1,
        E_StringType_i64 = 2,
        E_StringType_double = 3,
        E_StringType_String = 4
    };
    
    struct Argument
    {
    public:
        
        Argument( const xstl::String& Str );
        
        operator xstl::String() const
        {
            return m_Data;
        }
    
        static u64 StringProcess_u64( const xstl::String& Str );
        static i64 StringProcess_i64( const xstl::String& Str );
        static double StringProcess_double( const xstl::String& Str );
        static xstl::Array<Argument> SegmentArguments( const xstl::String& Str );
        static E_StringType StringType( const xstl::String& Str );
        
        const E_StringType m_Type;
        const xstl::String m_Data;
    };
    
    //====================================================================================
    //  Time
    //====================================================================================
    
    time_t GetTime(); //Datetime
    xstl::String GetTimeStr();
    
    xstl::String FormatTime( const time_t& TimeStamp ); //Format time_t according to preferences.
    
    //====================================================================================
    //  Engine Management
    //====================================================================================
    
    bool Init( void (*FuncLogOut)( const xstl::String& ) ); //Initializes the Engine. Pass the CLI TerminalDevice for log output.
    bool Deinit(); //Shutdown Engine
    
    void Crash( xstl::String Msg );
    
    i64 Status(); //Return Engine status. Returns -1 for Shutdown. 0 for normal.
    
    //====================================================================================
    //  Terminal Interface
    //====================================================================================
    
    void Log( const xstl::String& Msg, const xstl::String& SystemName = "Core", i64 Id = -1 );
    void ConsoleCommand( const xstl::String& CommandStr ); //Process a console command.
    
    //====================================================================================
    //  Machine Management
    //====================================================================================
    
    //====================================================================================
    //  Console Commands
    //====================================================================================
    
    // Check Argument array to make sure arguments align with command requirements.
    // Min = Mininum required arguments. 0 = No minimum. Max = Maximum amount of arguments, -1 = No max.
    // Types = Array of types that each argument is expected to have, in order. Last argument is used for every parameter after its index in array.
    bool CheckArgs( const xstl::String& CommandStr, const xstl::Array<Argument>& Args, const i64 Min = 0, const i64 Max = 0, 
                    const xstl::Array<E_StringType>& Types = { E_StringType_void } );
    
    xstl::String GetArgumentTypeList( const xstl::Array<Argument>& Args );
    
    void shutdown( const xstl::Array<Argument>& Args ); //0,0,0
    void crash( const xstl::Array<Argument>& Args ); //0,1,E_StringType_String
    void testmsg( const xstl::Array<Argument>& Args ); //0,1,E_StringType_String
    void threadtest( const xstl::Array<Argument>& Args ); //1,1,E_StringType_u64
    void printtestthreads( const xstl::Array<Argument>& Args ); //0,0,0
    void killtestthreads( const xstl::Array<Argument>& Args ); //0,-1,E_StringType_u64
    
    //====================================================================================
    //  Test Functions
    //====================================================================================
    
    struct TestThread
    {
        static xstl::Array<TestThread*> m_TestThreads;
        
        TestThread( u64 index );
        
        void SPAMBOI( ); //Log random strings at increasing intensity.
        
        double waitTimeMs = 5000;
        bool m_bKillMe = false;
        u64 id;
        std::thread* m_Handle;
    };
    
    i64 m_Status = 0;
    void (*m_FuncLogOut)( const xstl::String& ); //Where to return log output to. set by Init()
    
};
 
