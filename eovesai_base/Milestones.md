===========================
Eovesai Engine - Milestones
=========================== 

Miscellaneous:

    -USE Flags for optional native components/modules, system for detecting the availability of these
    modules at run-time.

    -Additionally, possibly some kind of framework for identifying classes at runtime that minimizes overhead.

Eovesai Terminal:

    User interface to the engine. Allows complete manipulation of local engine state.

    -CLI Interface, scrolling/rendering of log files, saving log files, internal commands *done

    -Input/Ouput interface (Logging and sending commands) *done

    -Threaded Logging *wip

Eovesai Core:

    -Command System *wip
        
        Parsing console input, identifying the command, and identifying the arguments by type.

    -Resource Manager

        Manages assets or other data into memory not specific to a particular machine.

        -Support for various formats, including files, textures, class references, models, sounds, etc.
        -Package system.

    -Machine API

        Various engine components, and interfaces for them to communicate speedily, and also registering them with
        core for further manipulation at runtime. Each Machine ideally runs on its own thread, however each machine may also define a method
        to parrellize its own tasks into more threads for acceleration.

        -Abstract Game Machine

            Abstract Machine for managing game modules, e.g., game-level dlls that provide native components.

        -Abstract Client Machine

            Abstract Client Machine that defines the concept initializing a graphical window, handling input, a UI environment.

        -Abstract Simulation Machine

            Abstract Simulation Machine, a virtual environment where objects or events may arbitrarily interact with eachother based on the rules
            of the simulation machine, used to establish game logic. Provides the API to facilitate this. May include a scripting language of sorts.

        -Abstract Video Machine

            Abstract Render device that communicates with Client and Simulation machines to render graphics.

        -Abstract Audio Machine

            Abstract Audio device that communicates with Client and Simulation to play audio.

        -Abstract Network device

            For connecting a Machine to an external Machine to exchange/synchronize data.

Kyrica Standard Modules:

    Provides the modules used for 'Kyrica'.

Eovesai Editor:

    Provides an editor to work with mapping or working with Engine packages.
